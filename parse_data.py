#!/usr/bin/env python3

import json
import re
from typing import List
import sys


servers = [
    "http01",
    "http02",
    "http03",
]


def find_apks(keys: List[str], appid: str) -> List[str]:
    pattern = r"^/repo/%s_\d+.apk$" % appid
    pattern = re.compile(pattern)

    apks = []
    for key in keys:
        if pattern.match(key):
            apks.append(key)
    return apks


# apps to print data for
if len(sys.argv) <= 1:
    print(f"Usage: {sys.argv[0]} <APP_FILE>")
    sys.exit(1)

target_apps_file = sys.argv[1]

with open(target_apps_file, "r") as f:
    target_apps = [line.strip() for line in f]


date_file = "raw_data/http02/index.json"
with open(date_file, "r") as f:
    dates = json.load(f)

for app in target_apps:
    app_downloads = []

    for date_json in dates:
        if date_json == "2024-09-30.json": continue
        download_count = 0

        for serv in servers:
            metrics_file = f"raw_data/{serv}/{date_json}"

            try:
                with open(metrics_file, "r") as f:
                    metrics_data = json.load(f)
            except FileNotFoundError:
                print(f"[WARN] {metrics_file} does not exist")
                continue

            # the data of the latest week seems to always be empty
            if not "paths" in metrics_data or len(metrics_data["paths"]) < 10:
                print(f"It looks like {serv}/{date_json} is empty, skipping")
                continue

            apks = find_apks(metrics_data["paths"].keys(), app)
            # print(f"{app}: {apks}")

            for apk in apks:
                download_count += metrics_data["paths"][apk]["hits"]
        print(
            f"{app} was download {download_count} times in the week of {date_json[:-5]}."
        )
        app_downloads.append([date_json[:-5], download_count])

    # Write data to file for consumption by plot_data.py
    app_downloads_file = f"app_downloads/{app}.json"
    with open(app_downloads_file, "w+") as f:
        try:
            downloads_old = json.load(f)
        except json.decoder.JSONDecodeError:
            downloads_old = {}

        for date, count in app_downloads:
            if date not in downloads_old:
                downloads_old[date] = count

        json.dump(downloads_old, f, indent=4, sort_keys=True)
