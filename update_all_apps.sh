#!/bin/bash

curl "https://f-droid.org/repo/index.xml" | xmllint --format - | grep "application id" | sed -e 's/ <application id="//' -e 's/">//' | sort > "all_apps.txt"
